
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.Factory;
import org.junit.Test;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory; 
public class MoneyTest {

@Test public void test() {
	System.out.println("Junit est bien install� "); 
}

/* On v�rifie que la fonction add aditionne bien le montant pour une m�me devise */
@Test public void moneyAddTest1() {
	Money m = new Money(100,"EUR");
	Money m1 = new Money(100,"EUR");
	assertTrue(m.add(m1)== 200);
}

/* On v�rifie que la fonction add n'aditionne pas 2 montant qui n'ont pas la m�me devise*/
@Test public void moneyAddTest1bis() {
	Money m = new Money(100,"USD");
	Money m1 = new Money(100,"EUR");
	assertTrue(	m.add(m1)== 0);
}

/* On v�rifie que la fonction add aditionne bien le montant que je lui met en param�tre avec le montant de mon instance */

@Test public void moneyAddTest2() {
	Money m = new Money(200,"EUR");
	assertTrue(m.add(100,"EUR")== 300);
}

/* On v�rifie que la fonction add n'aditionne pas 2 montant qui n'ont pas la m�me devise*/
@Test public void moneyAddTest2bis() {
	Money m = new Money(100,"USD");
	assertTrue(m.add(100,"EUR")== 0);
}


/* On v�rifie que l'on peut ajouter de l'argent peut importe la devise*/
@Test public void moneyAddTest3() {
	Money m = new Money(100,"USD");
	MoneyBag mb = new MoneyBag(200,"EUR");
	assertEquals(mb.add(m), 350);
}

/* On v�rifie que l'on peut retirer de l'argent peut importe la devise*/
@Test public void moneyAddTest3bis() {
	Money m = new Money(100,"USD");
	MoneyBag mb = new MoneyBag(200,"EUR");
	mb.subb(m);
	assertEquals(mb.getfAmount(), 50);
}

/*@TestFactory
Stream <DynamicTest> testFactory(){
	List <int[]> inputs = Arrays.asList(new int[] {1,2,50}, new int[] {2,3,50}, new int [] {3,4,50});
	List<Integer> results = Arrays.asList(2,2,50);
	Money m = new Money(100,"USD");
	MoneyBag mb = new MoneyBag(200,"EUR");
	mb.subb(m);

	return inputs.stream().mapToInt( termes -> DynamicTest.dynamicTest("Add" + termes, () -> {
			int i =inputs.indexOf(termes);
			assertEquals(results.get(i).intValue(),mb.getfAmount());
	}));
	
}*/

}
