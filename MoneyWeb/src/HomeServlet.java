

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
		if(request.getParameter("sommeA")!= null) {
			int sommeA= Integer.parseInt(request.getParameter("sommeA"));
			String deviseA= request.getParameter("deviseA");
			int sommeB= Integer.parseInt(request.getParameter("sommeB"));
			String deviseB= request.getParameter("deviseB");
			Money m = new Money(sommeA,deviseA);
			MoneyBag mb = new MoneyBag(sommeB,deviseB);
			int resultat= mb.add(m);
			System.out.println(resultat);
			request.setAttribute("result", resultat);
		}
		this.getServletContext().getRequestDispatcher( "/home.jsp" ).forward( request, response );
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
