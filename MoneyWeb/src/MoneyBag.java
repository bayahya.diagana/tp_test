import java.util.HashMap;
import java.util.Map;

public class MoneyBag {
	private int fAmount;
	private String fCurrency;
	private Map <String, Double> mDevise = new HashMap<String, Double>() ;
	private int fDevise;
	
	public MoneyBag(int fAmount, String fCurrency) {
		
		this.fAmount = fAmount;
		this.fCurrency = fCurrency;
		mDevise.put("EUR", 1.50 );
		mDevise.put("USD", 1.80 );
		mDevise.put("GBP", 2.10 );
		
	}
	
	public int getfAmount() {
		return fAmount;
	}

/*	public void setfAmount(int fAmount) {
		this.fAmount = fAmount;
	}*/

	public int add(Money m) {
		if(m.getfCurrency().equals(fCurrency)) {
			fAmount += m.getfAmount(); 
		}else {
		Double devise = mDevise.get(fCurrency);
		fAmount = (int) (fAmount + devise * m.getfAmount());
		}
		return fAmount;
	}
	public int subb(Money m) {
		Double devise = mDevise.get(fCurrency);
		fAmount = (int) (fAmount-devise * m.getfAmount());
		return fAmount;
	}

}
